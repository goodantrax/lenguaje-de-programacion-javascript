# Practica final del curso Lenguaje de programación JavaScript

## Autor

* Bermejo López Axel Nahir

### Contacto

axel_berm13@hotmail.com

## Instructor

* Mario Alberto Arredondo.

## Descripción

Esta práctica trata sobre mostrar resultados de busqueda de un catálogo de películas dados a partir de una API
a partir de una palabra o caracteres dados.

Para resolverlo hice uso de AJAX para crear una petición de tipo GET que me arrojara los resultados coincidentes, usando la URL de la API más el query de la palabra escrita en el input del formulario correspondiente.

